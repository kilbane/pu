!#/bin/sh
home=$(echo $HOME)
application=$({ echo new; find $home/.password-store/ -name *.gpg; } | sed -e "s|$home/.password-store/||g" -e 's|.gpg||g' | dmenu)
echo $application
if [[ ! -z $application ]]
then
  if [[ $application = "new" ]]
  then
    application=$(dmenu -p "What is the name of the new password?" < /dev/null)
    length=$(dmenu -p "How long should the password be? (Default: 25)" < /dev/null)
    export SSH_ASKPASS=pass_ssh
    pass generate $application $length > /dev/null
  else
    username=$(pass $application | sed -n '2p')
    password=$(pass $application | sed -n '1p')
    if [[ ! -z $password ]]
    then
      answer=$(echo -e "Copy\nEdit\nGenerate" | dmenu -p "What do you want to do with the password for $application?")
      if [[ $answer = "Copy" ]]
      then
        if [[ ! -z $username ]]
        then
          echo $username | xclip -i -selection clipboard -verbose -l 1 && \
          echo $password | xclip -i -selection clipboard -verbose -l 1
        else
          echo $password | xclip -i -selection clipboard -verbose -l 1
        fi
      elif [[ $answer = "Edit" ]]
      then
        st -e pass edit $application
      elif [[ $answer = "Generate" ]]
      then
        answer=$(echo -e "Yes\nNo" | dmenu -p "Are you sure you want to overwrite the password for $application?")
        if [[ $answer = "Yes" ]]
        then
          length=$(dmenu -p "How long should the password be? (Default: 25)" < /dev/null)
          export SSH_ASKPASS=pass_ssh
          pass generate $application $length > /dev/null
        fi
      fi
    else
      answer=$(echo -e "Yes\nNo" | dmenu -p "Do you want to generate a password for $application?")
      if [[ $answer = "Yes" ]]
      then
        length=$(dmenu -p "How long should the password be? (Default: 25)" < /dev/null)
        export SSH_ASKPASS=pass_ssh
        pass generate $application $length > /dev/null
      fi
    fi
  fi
fi
